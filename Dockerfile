#/var/www/html --> inside this directory we will place our website
#/etc/httpd will be created
#Port is there inside /etc/httpd/conf/httpd.conf
FROM centos
RUN yum install httpd -y
ARG PORT=80
WORKDIR /etc/httpd/conf/
RUN sed -i -e "/^Listen 80/ c Listen $PORT" httpd.conf
ENTRYPOINT [ "httpd","-DFOREGROUND" ]